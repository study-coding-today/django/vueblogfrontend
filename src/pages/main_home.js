import Vue from 'vue'
import App from './AppHome.vue'
import vuetify from '../plugins/vuetify';

Vue.config.productionTip = false

// vue instance 생성 
new Vue({
  vuetify,
  render: h => h(App)
}).$mount('#app') // DOM element 에 mount 되어 render 로 App.vue component 가 호출된다.
