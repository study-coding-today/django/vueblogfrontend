const FileManagerPlugin = require('filemanager-webpack-plugin-fixed');

module.exports = {
  "transpileDependencies": [
    "vuetify"
  ],

  devServer: {
    index: 'home.html',
  },

  // webpack 결과가 있을 디렉토리 default 와 같다.
  outputDir: 'dist',
  // webpack 들의 root url default 와 같다.
  publicPath: '/',
  assetsDir: 'static',
  // build 하게 되면 dist 에 새로 static 아래 구성된다.

  // 여러 html 을 생성하려면 pages 를 사용한다.
  pages: {
    home: {
      template: 'public/index.html',   // home.html 를 만들때 참조하는 template
      entry: 'src/pages/main_home.js', // webpack build 시작점
      filename: 'home.html',           // 최종결과 
      title: 'VueDjangoPhoto/home.html',
      minify: false,
    },
    post_list: {
      template: 'public/index.html',   // home.html 를 만들때 참조하는 template
      entry: 'src/pages/main_post_list.js', // webpack build 시작점
      filename: 'post_list.html',           // 최종결과 
      title: 'VueDjangoPhoto/post_list.html',
      minify: false,
    },
    post_detail: {
      template: 'public/index.html',   // home.html 를 만들때 참조하는 template
      entry: 'src/pages/main_post_detail.js', // webpack build 시작점
      filename: 'post_detail.html',           // 최종결과 
      title: 'VueDjangoPhoto/post_detail.html',
      minify: false,
    },
    
  },
  configureWebpack: {
    plugins: [
      new FileManagerPlugin({
        onStart: {
          delete: [
            '../vueblog/static/**/',
            '../vueblog/templates/**/',
          ],
        },

        onEnd: {
          copy: [
            { source: 'dist/static', destination: '../vueblog/static/' },
            { source: 'dist/favicon.ico', destination: '../vueblog/static/img/' },
            { source: 'dist/home.html', destination: '../vueblog/templates/' },
            { source: 'dist/post*.html', destination: '../vueblog/templates/blog/' },
          ],
        }
      }),
    ]
  },
}